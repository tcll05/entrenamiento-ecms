import { Component, OnInit } from '@angular/core';
import { AuthGuard } from './Services/auth.guard';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers:[AuthGuard]
})
export class AppComponent implements OnInit {
    ngOnInit(): void {
        this.sesion = sessionStorage.getItem('isLoggedIn');
        if(this.sesion != null)
            this.sesionIniciada=true;
            else this.sesionIniciada=false;
    }
    sesion:string;
    sesionIniciada:boolean;
    title = 'entrenamiento-ecms';

}
