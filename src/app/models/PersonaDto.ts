export class PersonaDto {
    personaId: number;
    identificacion: string;
    primerNombre: string;
    primerApellido: string;
    fechaNacimiento:Date;
    nacionalidad: string;
    genero: string;
}
