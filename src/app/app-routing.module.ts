import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './Services/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  {
    path: "home",
    loadChildren: () =>
      import("./Components/home/home.module").then(m => m.HomeModule),
      canActivate:[AuthGuard]
  },
  {
    path: "personas",
    loadChildren: () =>
      import("./Components/personas/personas.module").then(m => m.PersonasModule),
      canActivate:[AuthGuard]
  },
  {
    path: "",
    loadChildren: () =>
      import("./Components/usuarios/usuarios.module").then(m => m.UsuariosModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[AuthGuard]
})
export class AppRoutingModule { }
