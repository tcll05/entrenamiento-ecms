import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PersonaDto } from 'src/app/models/PersonaDto';
@Component({
    selector: 'app-crear-persona',
    templateUrl: './crear-persona.component.html',
    styleUrls: ['./crear-persona.component.css']
})
export class CrearPersonaComponent implements OnInit {
    formPersona: FormGroup;
    faCalendar = faCalendar;
    persona: PersonaDto;
    //Variable de entrada para modificar una persona registrada
    @Input() public tmpPersona: PersonaDto;
    //variable de salida para retornar la persona que se registro
    @Output() passEntry: EventEmitter<PersonaDto> = new EventEmitter();
    constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal) { }

    ngOnInit(): void {
        this.formPersona = this.formBuilder.group({
            identificacion: [null, [Validators.required, Validators.maxLength(20)]],
            primerNombre: [null, [Validators.required, Validators.maxLength(50)]],
            primerApellido: [null, [Validators.required, Validators.maxLength(50)]],
            fechaNacimiento: [Date, [Validators.required]],
            nacionalidad: [null, [Validators.required, Validators.maxLength(50)]],
            genero: [null, [Validators.required, Validators.maxLength(1)]]
        })
        // si se esta modificando una persona, se formatea la fecha para poder ser visualizada
        if (this.tmpPersona != null) {
            let fechaTemp = new Date(this.tmpPersona.fechaNacimiento)
            let tmpDate = {
                "year": fechaTemp.getFullYear(),
                "month": fechaTemp.getMonth() + 1,
                "day": fechaTemp.getDate()
            };
            //al formulario se le asignan los datos recibidos en tmpPersona
            this.formPersona.patchValue(this.tmpPersona);
            this.formPersona.patchValue({ "fechaNacimiento": tmpDate })
        }
    }

    guardar() {
        /*si el formulario es invalido (si no cumple con todas las validaciones)
        * muestra los mensajes de validacion         
        */
        if (this.formPersona.invalid) {
            debugger;
            this.formPersona.markAllAsTouched();
            return;
        }
        else if(this.validarGenero())
        {
            this.formPersona.markAllAsTouched();
            return;
        }
        else {
            // al modelo persona le asigna los valores del formulario (por esta razon los nombres deben ser iguales)
            this.persona = this.formPersona.value;
            //si la variable de entrada tmpPersona (modificar) no es nula, 
            //le asigna el id al objeto persona que sera registrado
            if (this.tmpPersona != null)
                this.persona.personaId = this.tmpPersona.personaId;
            // retornamos a la pantalla de listado el objeto persona con todos los datos que fueron ingresados
            this.passEntry.emit(this.persona);
            //se vacian los campos y se cierra la modal
            this.formPersona.reset();
            this.activeModal.close();
        }

    }
    //valida que el genero seleccionado sea correcto
    validarGenero() {
        if (this.formPersona.get('genero').value == "F" || this.formPersona.get('genero').value == "M")
            return false;
        else return true;
    }

}
