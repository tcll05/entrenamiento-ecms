import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasRoutingModule } from './personas-routing.module';
import { CrearPersonaComponent } from './crear-persona/crear-persona.component';
import { ModificarPersonaComponent } from './modificar-persona/modificar-persona.component';
import { ListaPersonasComponent } from './lista-personas/lista-personas.component';
import { IndexPersonasComponent } from './index-personas/index-personas.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CrearPersonaComponent, ModificarPersonaComponent, ListaPersonasComponent, IndexPersonasComponent],
  imports: [
    CommonModule,
    PersonasRoutingModule,
    FontAwesomeModule,
    NgbDatepickerModule,
    
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents:[
    CrearPersonaComponent
  ]
})
export class PersonasModule { }
