import { Component, OnInit, DebugElement } from '@angular/core';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CrearPersonaComponent } from '../crear-persona/crear-persona.component';
import { PersonaDto } from 'src/app/models/PersonaDto';
import { PeticionService } from 'src/app/Services/peticion.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
    selector: 'app-lista-personas',
    templateUrl: './lista-personas.component.html',
    styleUrls: ['./lista-personas.component.css']
})
export class ListaPersonasComponent implements OnInit {
    data: PersonaDto[];
    //variable que sera enviada a la API para ser registrada / modificada
    savePersona: PersonaDto;
    faPencilAlt = faPencilAlt;
    //hacemos referencia al servicio de NgbModal
    constructor(private modalService: NgbModal, private peticion: PeticionService) { }

    ngOnInit(): void {
        this.data = [];
        this.getData();
    }

    getData() {
        this.peticion.getAll<PersonaDto[]>(`persona/`).subscribe(
            result => { this.data = result; },
            error => { }
        )
    }

    nuevo() {
        //hacemos el llamado para abrir el modal
        const modalRef = this.modalService.open(CrearPersonaComponent);
        //nos suscribimos para recibir los datos registrados en el formulario
        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                //a la variable de registro le asignamos el valor que recibimos del formulario
                this.savePersona = receivedEntry;
                //formateamos la fecha yyyy/MM/dd
                this.savePersona.fechaNacimiento = new Date(`${receivedEntry.fechaNacimiento.year}/${receivedEntry.fechaNacimiento.month}/${receivedEntry.fechaNacimiento.day}`);
                //hacemos el post mediante el servicio de peticion
                this.peticion.post<any>("persona/", this.savePersona).subscribe(
                    result => {
                        if (result.resultado == true) { //si el resultado es correcto
                            this.data.push(this.savePersona); //agregamos el registro al listado
                            Swal.fire(
                                'Registro agregado!',
                                'El registro ha sido creado con exito',
                                'success'
                            )
                        }
                        else{
                            Swal.fire(
                                'Error!',
                                result.mensaje,
                                'error'
                            )
                        }

                    },
                    error => {
                        Swal.fire(
                            'Error!',
                            'Ha ocurrido un error al crear el registro',
                            'error'
                        )
                    }
                )

            })
    }
    
    editar(persona: PersonaDto) {
        const modalRef = this.modalService.open(CrearPersonaComponent);
        //enviamos la persona a modificar al componente de crear-persona
        modalRef.componentInstance.tmpPersona = persona;

        modalRef.componentInstance.passEntry.subscribe(
            (receivedEntry) => {
                this.savePersona = receivedEntry;
                this.savePersona.fechaNacimiento = new Date(`${receivedEntry.fechaNacimiento.year}/${receivedEntry.fechaNacimiento.month}/${receivedEntry.fechaNacimiento.day}`);
                //enviamos mediante el metodo put el registro a modificar
                this.peticion.put<boolean>("persona", this.savePersona, receivedEntry.personaId).subscribe(
                    result => {
                        if (result == true) {
                            let index = this.data.findIndex(item => item.personaId == this.savePersona.personaId);
                            this.data[index] = this.savePersona;
                            Swal.fire(
                                'Registro modificado!',
                                'El registro ha sido modificado con exito',
                                'success'
                            )
                        }
                        else {
                            Swal.fire(
                                'Error',
                                'El registro no fue modificado...',
                                'error'
                            )
                        }
                    },
                    error => {
                        Swal.fire(
                            'Error',
                            'Error al modificar el registro!',
                            'error'
                        )
                    }
                )

                //this.data.push(this.savePersona);

            })
    }

    eliminar(persona: PersonaDto) {
        Swal.fire({
            title: 'Eliminar Registro',
            text: `Seguro desea eliminar el registro de ${persona.primerNombre} ${persona.primerApellido}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                //enviamos el id del registro a ser eliminado
                this.peticion.delete<boolean>("persona", persona.personaId).subscribe(
                    result => {
                        if (result == true) {
                            Swal.fire(
                                'Registro Eliminado!',
                                'El registro ha sido eliminado con exito',
                                'success'
                            )
                        }
                        else {
                            Swal.fire(
                                'Error',
                                'El registro no fue eliminado...',
                                'error'
                            )
                        }
                    },
                    error => {
                        console.log(error);
                    }
                )

            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        })
    }

    buscar(buscarText:string) {
        if (buscarText == '')
            return this.data;

        return this.data.filter(persona => 
            persona.primerNombre.concat(` ${persona.primerApellido}`).indexOf(buscarText.toUpperCase()) > -1);
    }
}
