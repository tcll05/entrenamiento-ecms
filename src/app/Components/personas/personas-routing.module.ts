import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPersonasComponent } from './index-personas/index-personas.component';
import { AuthGuard } from 'src/app/Services/auth.guard';


const routes: Routes = [
  {
    path: '',
    component: IndexPersonasComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonasRoutingModule { }
