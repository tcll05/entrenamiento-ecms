import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPersonasComponent } from './index-personas.component';

describe('IndexPersonasComponent', () => {
  let component: IndexPersonasComponent;
  let fixture: ComponentFixture<IndexPersonasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexPersonasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPersonasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
