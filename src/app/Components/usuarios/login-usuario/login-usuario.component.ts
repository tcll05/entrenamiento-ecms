import { Component, OnInit } from '@angular/core';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import { PeticionService } from 'src/app/Services/peticion.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
@Component({
    selector: 'app-login-usuario',
    templateUrl: './login-usuario.component.html',
    styleUrls: ['./login-usuario.component.css']
})
export class LoginUsuarioComponent implements OnInit {
    faUser = faUser;
    faLock = faLock;

    usuario: string;
    contrasenia: string;
    formInicio:FormGroup;
    constructor(private peticion:PeticionService, private builder:FormBuilder, private router: Router, private sesion:AuthService) { }

    ngOnInit(): void {
        this.formInicio = this.builder.group({
            nombre:['',[Validators.required]],
            contrasenia:['',[Validators.required]]
        })
    }

    iniciarSesion() {
        if(this.formInicio.invalid)
        {
            this.formInicio.markAllAsTouched();
            return;
        }
        else{
            var formData = new FormData();
            formData.append("nombre", this.formInicio.get('nombre').value);
            formData.append("contrasenia", this.formInicio.get('contrasenia').value);
            this.peticion.postLogin<any>("cuenta/autenticar", formData).subscribe(
                result => {
                    if (result.resultado == false) {
                        Swal.fire(
                            'Error',
                            result.mensaje,
                            'error'
                        )
                        sessionStorage.removeItem("isLoggedIn");
                    }
                    else {
                        this.sesion.login(result.token);
                        this.router.navigate(['/personas']);
                    }
    
                },
                error => {
                    Swal.fire(
                        'Error',
                        error.error.mensaje,
                        'error'
                    )
                    sessionStorage.removeItem('isLoggedIn');
                }
            );
        }
        

    }


}
