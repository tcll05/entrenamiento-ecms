import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioDto } from 'src/app/models/UsuarioDto';
import { PeticionService } from 'src/app/Services/peticion.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
    selector: 'app-registro-usuario',
    templateUrl: './registro-usuario.component.html',
    styleUrls: ['./registro-usuario.component.css']
})
export class RegistroUsuarioComponent implements OnInit {
    formPersona: FormGroup;
    usuario: UsuarioDto;
    constructor(private formBuilder: FormBuilder,private peticion:PeticionService) { }

    ngOnInit(): void {
        this.formPersona = this.formBuilder.group({
            nombre: [null,[Validators.required]],
            correoElectronico: [null, [Validators.required, Validators.email]],
            contrasenia: [null, [Validators.required, Validators.minLength(8)]],
            contrasenia2:[null, [Validators.required, Validators.minLength(8)]]
        })
    }

    guardar()
    {
        if(this.formPersona.invalid)
        {
            this.formPersona.markAllAsTouched();
            return;
        }
        else
        {
            this.usuario = this.formPersona.value; // cuenta/autenticar
            var formData = new FormData();
            formData.append("nombre",this.usuario.nombre);
            formData.append("correoElectronico",this.usuario.correoElectronico);
            formData.append("contrasenia",this.usuario.contrasenia);
            this.peticion.postLogin<any>("cuenta/registrar",formData).subscribe(
                result=>{
                    if(result.resultado == false)
                    {
                        Swal.fire(
                            'Error',
                            result.mensaje,
                            'error'
                        )
                        
                    }
                    else {
                        Swal.fire(
                            'Registro Creado!',
                            'El registro ha sido guardado con exito',
                            'success'
                        )
                        this.formPersona.reset();
                    }
                    
                },
                error=>{
                    Swal.fire(
                        'Error',
                        error.error.mensaje,
                        'error'
                    )
                }
            );
        }
        
    }

    validarContrasenia():boolean{
        if(this.formPersona.get('contrasenia').value == this.formPersona.get('contrasenia2').value) return false;
        else return true;
    }

}
