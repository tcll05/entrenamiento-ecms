import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistroUsuarioComponent } from './registro-usuario/registro-usuario.component';
import { LoginUsuarioComponent } from './login-usuario/login-usuario.component';
import { AuthGuard } from 'src/app/Services/auth.guard';


const routes: Routes = [
  {
    path: 'registro',
    component: RegistroUsuarioComponent
  },
  {
    path: '',
    component: LoginUsuarioComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
