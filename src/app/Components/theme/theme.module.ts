import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThemeRoutingModule } from './theme-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { UsuariosModule } from '../usuarios/usuarios.module';


@NgModule({
  declarations: [NavbarComponent],
  imports: [
    CommonModule,
    ThemeRoutingModule,
    NgbCollapseModule,
    UsuariosModule
  ],
  exports: [
    NavbarComponent,
    UsuariosModule,
  ]
})
export class ThemeModule { }
