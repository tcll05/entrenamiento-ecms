import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public isMenuCollapsed = true;
  constructor(private router: Router, private sesion:AuthService) { }

  isLoggedIn$: Observable<boolean>; 

  ngOnInit(): void {
    this.isLoggedIn$ = this.sesion.isLoggedIn;
  }

  cerrarSesion()
  {
    this.isMenuCollapsed=true;
    this.sesion.logout();
    this.router.navigate(['/'])
  }

}
